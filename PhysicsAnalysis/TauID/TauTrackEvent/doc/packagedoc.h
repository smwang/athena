/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

/**
@page TauTrackEvent_page TauTrackEvent package
 * The EDM class package for TauTrackTools.
 *
 * @author Sebastian.Fleischmann@cern.ch
 *
@section TauTrackEvent_@section introductionTauTrackEvent Introduction
 *
 * This package contains the data classes for TauTrackTools.
 *
@section TauTrackEvent_@section TauTrackEventOverview Class Overview
 *   The TauTrackEvent package contains the following classes:
 *
 *     - TauID::TruthTau : Special class for tau truth data (inherits from TruthParticle)
 *     - TauID::TauJetTruthMap : Map between Analysis::TauJet and TauID::TruthTau
 *
@section TauTrackEvent_@section ExtrasTauTrackEvent Extra Pages
 *
 *       */

/**
 * */

/**
 * */
